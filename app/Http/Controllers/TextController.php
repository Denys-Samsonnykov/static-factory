<?php

namespace App\Http\Controllers;

use App\Http\Requests\KeysRequest;
use App\Http\Requests\TagsRequest;
use App\Services\TextService;
use Illuminate\Http\JsonResponse;

class TextController extends Controller
{
    protected TextService $textService;

    public function __construct(TextService $textService)
    {
        $this->textService = $textService;
    }

    public function filterTags(TagsRequest $request):JsonResponse
    {
        $array = $this->textService->getTagsArray($request->text);

        return response()->json(['tags' => $array]);
    }

    public function filterKeys(KeysRequest $request):JsonResponse
    {
        $array = $this->textService->getKeysArray($request->text, $request->keys);

        return response()->json(['keys' => $array]);
    }
}
