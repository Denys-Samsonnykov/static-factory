<?php

namespace App\Services;

use DOMDocument;

class TextService
{
    public function getTagsArray(string $text): array
    {
        $dom = new DOMDocument;
        libxml_use_internal_errors(true);
        $dom->loadHTML($text, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        libxml_use_internal_errors(false);

        $tagDataArray = [];
        $tagDescriptionArray = [];

        foreach ($dom->getElementsByTagName('*') as $tag) {
            $tagName = $tag->tagName;
            $tagData = $tag->nodeValue;
            $tagDescription = utf8_decode($tag->getAttribute('description'));

            if ($tagDescription !== '') {
                $tagDescriptionArray[$tagName] = $tagDescription;
            }

            $tagDataArray[$tagName] = $tagData;
        }

        return ['data' => $tagDataArray, 'description' => $tagDescriptionArray];
    }

    public function getKeysArray(string $text, string $stringKeys): array
    {
        $keysForPattern  = $this->prepareKeys($stringKeys);

        $matches = [];
        preg_match_all("/\b($keysForPattern)\s+(.*?)(?=\s+(?:$keysForPattern)\s+|$)/i", $text, $matches);

        $keys = array_values(array_filter($matches[1]));
        $values = array_map('trim', array_values(array_filter($matches[2])));

        return array_combine($keys, $values);
    }

    public function prepareKeys(string $keys): string
    {
        $keysArray = explode(',', $keys);

        $keysArray = array_map(function($key) {
            return trim($key);
        }, $keysArray);

        return implode('|', $keysArray);
    }
}