<?php

use App\Http\Controllers\CostController;
use App\Http\Controllers\TextController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
//
//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::controller(CostController::class)->name('cost.')->group(function() {
    Route::post('/cost', 'calculate')->name('calculate');
});

Route::controller(TextController::class)->name('text.')->group(function() {
    Route::post('/text/tags', 'filterTags')->name('filterTags');
    Route::post('/text/keys', 'filterKeys')->name('filterKeys');
});
