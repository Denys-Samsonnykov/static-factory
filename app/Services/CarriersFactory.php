<?php

namespace App\Services;

use App\Helpers\Contracts\CarrierInterface;
use App\Models\PackGroup;
use App\Models\TransCompany;

class CarriersFactory
{
    public static function create(string $title): CarrierInterface
    {
        switch ($title) {
            case 'TransCompany':
                $carrier = new TransCompany();
                break;

            case 'PackGroup':
                $carrier = new PackGroup();
                break;
        }
        return $carrier;
    }
}