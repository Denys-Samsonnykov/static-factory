<?php

namespace App\Http\Controllers;

use App\Http\Requests\CostRequest;
use App\Services\CarriersFactory;

class CostController extends Controller
{
    public function calculate(CostRequest $request)
    {
        $carrier = CarriersFactory::create($request->title);
        $cost = $carrier->transportationCost($request->mass);
        return response()->json(['cost' => $cost]);
    }
}
