<?php

namespace App\Models;

use App\Helpers\Contracts\CarrierInterface;

class PackGroup implements CarrierInterface
{
    public function transportationCost(float $mass): int
    {
        return (int) $mass;
    }
}
