<?php

namespace App\Helpers\Contracts;

interface CarrierInterface
{
    public function transportationCost(float $mass): int;
}