<?php

namespace App\Models;

use App\Helpers\Contracts\CarrierInterface;

class TransCompany implements CarrierInterface
{
    public function transportationCost(float $mass): int
    {
        return $mass <= 10 ? 20 : 100;
    }
}
