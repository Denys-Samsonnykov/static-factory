<?php

namespace App\Enums;

enum CompanyEnum:string
{
    case TRANS_COMPANY = 'TransCompany';
    case PACK_GROUP = 'PackGroup';
}
